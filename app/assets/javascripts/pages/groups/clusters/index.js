import initCreateCluster from '~/create_cluster/init_create_cluster';
import initIntegrationForm from '~/clusters/forms/show/index';

initCreateCluster(document, gon);
initIntegrationForm();
